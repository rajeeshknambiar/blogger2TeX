#!/usr/bin/perl
#
#Extract blogger blog pages from sitemap.xml and convert to TeX source
#
#Copyright 2013-2016 Rajeesh K V <rajeeshknambiar@gmail.com>
#License: GNU GPLv3
#

use utf8;
use strict; use warnings;

use File::Spec; use File::Basename; use File::Path qw(make_path);
use HTML::TreeBuilder;
use LWP::Simple;
use re;

# Read the sitemap file (and also get the toplevel directory)
$ARGV[0] or die "Give me the blogger site, please!\n";
my $sitemapurl = $ARGV[0] . "/sitemap.xml";
my $directory;
if ($ARGV[1]) {
    $directory = $ARGV[1];
    }
else {
    print "No directory given, creating 'blogbookdir' \n";
    $directory = File::Spec->curdir()."/blogbookdir";
    make_path($directory);
    }
#my ($fname, $dirname, $suffix) = fileparse(File::Spec->rel2abs($directory));
my $dirname = File::Spec->rel2abs($directory);
#print "dirname: $dirname \n";
open LOGFILE, '>:encoding(UTF-8)', "$dirname/filelist.txt" or die "Couldn't create list of files generated";

my $htmlurl;
foreach $htmlurl (get_htmlurl_from_sitemap($sitemapurl)) {
    #print "$htmlurl \n";
    html_to_TeX( $htmlurl );
}

#close(SITEMAP);
close(LOGFILE);
print "Generated files list can be found in $dirname/filelist.txt\n";

#--------------------------------------------------------------------------------
# Get all html urls from sitemap XML
sub get_htmlurl_from_sitemap {
    my @htmlurls;
    my $sitemap = $_[0];   # parameter 0 like: http://anonyantony.blogspot.com/sitemap.xml
    print "Getting sitemap: $sitemap\n";
    my $ua = LWP::UserAgent->new;
    my $response = $ua->get($sitemap);
    if ($response->is_success) {
        my $sitemap_content = $response->decoded_content;
        my @loc_urls = $sitemap_content =~ m|<loc>([^<]+)</loc>|g;
        my $locurl;
        foreach $locurl (@loc_urls) {
            if ( $locurl =~ /sitemap.xml/ ){
                push(@htmlurls, get_htmlurl_from_sitemap($locurl)); #another sitemap url, recurse
                }
            elsif ( $locurl =~ /\.html/ ) {
                push(@htmlurls, $locurl);  #html url, put in the array
                }
        }
    } else {
        die "Error fetching sitemap: $! \n";
    }
    return @htmlurls;
}

#--------------------------------------------------------------------------------
# Retrieve the HTML content, parse and convert to TeX format
sub html_to_TeX {
    my $url = $_[0];    #parameter 0 - they are in array @_
    #my $url = "http://anonyantony.blogspot.com/2012/04/blog-post.html";
    return unless $url =~ m|(https?://.+\.html)|;
    $url = $1;		#Paranthesized matching string from line above
    my $month, my $year, my $title;
    if($url =~ m|([0-9]{4})/([0-9]{2})| ) {  #Year/Month
        $year = $1; $month=$2;
    };
    if ($url =~ m|/([^/]+).html|) {	#String after last / and before .html
        $title = $1;
    }
    
    unless (-d "$dirname/$year/$month") {
        unless (-d "$dirname/$year") {
            mkdir "$dirname/$year" or die "$!";
        }
        mkdir "$dirname/$year/$month" or die "$!";
    }

    print "Retreiving and formatting $url\n";
    my $tree = HTML::TreeBuilder->new_from_url($url);
    #$tree->parse();
    #TODO: Tags could vary, adjust/expand if needed
    my $header = $tree->look_down( _tag => 'h3', 'class' => 'post-title entry-title');
    if (! $header ) {
        $header = $tree->look_down( _tag => 'h3', 'class' => 'post-title');
    }
    if ($header) {
        $header = $header->as_text();
    }
    else {
        print "Error: No header/title found for $url\n";
    }
    $header =~ s|([_\$&#%^])|\\$1|g;   #Escape special characters in header
    my $post_date = $tree->look_down( _tag => 'h2', 'class' => 'date-header')->as_text();
    my $body_text = $tree->look_down( _tag => 'div', 'class' => 'post-body entry-content');
    if (! $body_text ) {
        $body_text = $tree->look_down( _tag => 'div', 'class' => 'post-body');
    }
    if ($body_text) {
        $body_text = $body_text->as_XML();
    }
    else {
        print "Error: No body text found, skipping $url\n";
        return;
    }
    #print $body_text;

    #my $body_text = $body_html->look_down( _tag => 'div', 'dir' => 'ltr')->as_XML();
    # Capture the images
    #my $orig_file = "$dirname/$year/$month/$title.txt";
    #open ORIG_FILE, '>:encoding(UTF-8)', $orig_file;
    #print ORIG_FILE "$body_text";
    #close(ORIG_FILE);
    
    my @images=($body_text =~ /<img[^>]+src="([^"]+)"[^>]+>/g); #Images? NOTE: if it doesn't work, try my ($images) - list
    my $imageurl, my $imagefile, my $lastslash, my @savedimages, my $index=0;
    foreach $imageurl (@images) {
        $index++;
        #print "Image url: $imageurl\n";
        next unless $imageurl =~ /jpe?g|png/i; #case-insensitive match for images
        my $lastslash = rindex($imageurl, "/") + 1;
        my $imagename = substr($imageurl, $lastslash);
        #Check if the same filename (can happen as we take only the last part in url) already used
        if ( grep(/$imagename/, @savedimages) ) { #Scalar lookp ($imagename ~~ @savedimages) - Perl 5.10 OR use grep (/$imagename/,@savedimages)
            $imagename = $imagename."-".$index; # Add counter to the name
        }
        my $imagefile = "$dirname/$year/$month/$title-$imagename"; #title-imagename
        print "Retrieving image $imageurl to file $imagefile\n";
        getstore($imageurl, $imagefile) or die "$!\n";
        push(@savedimages, $imagename);
        #print "saved images: @savedimages \n";
        $body_text = convert_images_to_TeX($body_text,$year,$month,$title,$imagename);
    }
    
    $body_text = convert_html_entities_to_TeX($body_text);  #&quot;&apos;&amp;&gt;&lt;NBSP
    
    $body_text = convert_br_to_par($body_text);   #<br /> => \par
        
    $body_text = convert_href_to_TeX($body_text);   # <a href="...">...</a> => \href{...}{...}
    
    $body_text = convert_font_effect($body_text);   # Bold/Italic
    
    $body_text =~ s|<[^>]+>||g;		#Remove rest of <...> tags
    
    $body_text = escape_special_chars($body_text);  #Underscore, $, &, #, %, +, -, =, <, >, ^ 
    
    $body_text = unescape_image_underscore($body_text); #Un-escape \_ in \includegraphics
    
    $body_text =~ s|Posted by||g;   #Remove the stray 'Posted by' text
        
    #If there's still unescaped underscore left, log where it is
    #if ($body_text =~ /[^\\]_/) {
    #    print "$body_text\n";
    #}

    my $tex_file = "$dirname/$year/$month/$title.tex";
    open TEX_FILE, '>:encoding(UTF-8)', $tex_file or print "Can't open $tex_file for writing\n" and next;
    
    print TEX_FILE "\\section{$header}\n\n";
    print TEX_FILE "$body_text";
    my $escaped_url = $url; $escaped_url =~ s|_|\\_|g;
    print TEX_FILE "\n\n\\begin{flushright}\\href{$escaped_url}{\\sffamily $post_date}\\end{flushright}\n"; #\\begin{flushright} $post_date \end{flushright}\n
    print TEX_FILE "\\newpage\n";

    close(TEX_FILE);
    print LOGFILE "$year/$month/$title.tex\n"; #Converted and saved TeX file
    $tree->delete;
}

#--------------------------------------------------------------------------------

sub convert_images_to_TeX {
    my $text_to_convert = shift;    # $body_text Argument
    my $year = shift;   #Year
    my $month = shift;  #Month
    my $title = shift;  #Post title
    my $imagename = shift;  #Image name
    
    # Get the width and height attributes of image, if specified
    my ($width) = ($text_to_convert =~ /<img[^>]*width[ =:"]*(\d+)[^>]+>/);
    $width *= 0.6; # print "width: $width \n";  # px to pt conversion : 1 px = 0.75 pt
    my ($height) = ($text_to_convert =~ m/<img[^>]*height[ =:"]*(\d+)[^>]+>/);
    $height *=  0.6; # print "height: $height \n";
    # Construct the regex to replace <img...> with \includegraphics, with optional width/height.
    my $img_regex = "\n\\includegraphics";
    my $img_wh = $width  ? "width=$width"."pt" : "" ;
    $img_wh = $height ? ( $img_wh? $img_wh.",height=$height"."pt" : "height=$height"."pt" ) : $img_wh;
    $img_regex = $img_wh ? "$img_regex"."[$img_wh]" : "$img_regex"."[width=320pt]" ; #default width
    #print "regex: $img_regex \n";
    $img_regex = qr/$img_regex/;  #construct a regex variable that can be used below
    $img_regex =~ s|\(\?\^\:([^\)]+)\)|$1|;   #Remove (?^: and ) added by 'qr'
    #print "compiled regex: $img_regex \n";
    # Replace <img...> with \includegraphics[width=...,height=...]{src/img}
    $text_to_convert =~ s|<img[^>]+src="([^"]+)"[^>]+>|$img_regex\{$year/$month/$title-$imagename\}|;	#Just the image name (after last / in src)
    #$text_to_convert =~ s|<img[^>]+src="([^"]+)"[^>]+>|\n\\includegraphics[width=$width][height=$height]{$year/$month/$title-$imagename}\n|g;	#Just the image name (after last / in src)
    
    return $text_to_convert;
}

sub convert_html_entities_to_TeX{
    my $text_to_convert = shift;    # $body_text Argument
    
    $text_to_convert =~ s|&quot;([\w+])|“$1|g;#Left double quote
    $text_to_convert =~ s|&quot;(\s*)|”$1|g;  #Right double quote
    $text_to_convert =~ s|&apos;([\w+])|‘$1|g;#Left single quote
    $text_to_convert =~ s|&apos;(\s*)|’$1|g;  #Right signle quote
    $text_to_convert =~ s|&amp;|\&|g;	      #Ampersand
    $text_to_convert =~ s|&gt;|>|g;	      #Greater than
    $text_to_convert =~ s|&lt;|<|g;	      #Lesser than
    $text_to_convert =~ s| | |g;              #NBSP <non-break space> with SPACE
    
    return $text_to_convert;
}

sub convert_br_to_par{
    my $text_to_convert = shift;    # $body_text Argument
    $text_to_convert =~ s|<br />| \\par\n|g;	#Replace <br /> with \par
    return $text_to_convert;
}

sub convert_href_to_TeX{
    my $text_to_convert = shift;    # $body_text Argument
    $text_to_convert =~ s|<a href="([^"]+)">([^<]+)</a>|"\\href{$1}{\\ttfamily". convert_tilde($2)."}"|ge;   #Links: <a href="url">text</a> => \href{url}{text}
    return $text_to_convert;
}

sub convert_font_effect{
    my $text_to_convert = shift;    # $body_text Argument
    
    $text_to_convert =~ s@(<strong>|<b>)([^<]+)(</strong>|</b>)@\\textbf{$2}@g;#Bold: (<strong>|<b>)text(</strong>|</b>) => \textbf{text}
    $text_to_convert =~ s@(<em>|<i>)([^<]+)(</em>|</i>)@\\textit{$2}@g;    #Italic: (<em>|<i>)text(</em>|</i>)   => \textit{text}
    return $text_to_convert;
}

sub escape_special_chars{
    my $text_to_convert = shift;    # $body_text Argument
    $text_to_convert =~ s|([_\$&#%^])|\\$1|g;   #Underscore, $, &, #, %, +, -, =, <, >, ^ 
    return $text_to_convert;
}

sub unescape_image_underscore{
    my $text_to_convert = shift;    # $body_text Argument
    #NOTE: underscore in includegraphics wreck havoc, manually fix it up)
    #Replace escaped underscores only in the includegraphics path parameter.
    #Test case: href=http://blogspot.be/\_OTug/SpC\_zlI/s-h/TVM\_Lodge.jpg}{\includegraphics[width=240pt]{02/blog-post-TVM\_Lodge.jpg}}
    #Subroutine and eval are used to make this work
    $text_to_convert =~ s|(includegraphics[^{]*){([^}]+)}|$1."{".convert_underscore($2)."}"|ge; #Replace \_ in includegraphics with _
    return $text_to_convert;
}

sub convert_underscore {
    my $text_to_convert = shift;    #Argument
    $text_to_convert =~ s|\\_|_|g;  # \_ => _
    return $text_to_convert;
}

sub convert_tilde {
    my $text_to_convert = shift;    #Argument
    $text_to_convert =~ s|~|\~{}|g;  # ~ => \~{} -- Tilde is to be escaped along with {}
    return $text_to_convert;
}
