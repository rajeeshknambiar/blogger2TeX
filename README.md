blogger2TeX
===========

blogger2TeX is a `Perl` script that downloads contents of a
blog (hosted by blogger) and converts blog posts from html
to TeX format. It is highly experimental and only tested
with a handful of blogs. The script would need adjustment
depending on the blogger theme, especially for html tags
header (h2,h3...) and content (div 'content').

Usage
-----
`$ blogger2TeX "http://myblog.blogspot.com" [blogbookdir]`

Downloaded files will be listed in `blogbookdir/filelist.txt`

Implementation details
----------------------
List of blog post urls are first downloaded from
"http://myblog.blogspot.com/sitemap.xml" using `LWP::Simple`.
Each blog url (html) is then downloaded into `blogbookdir/year/month`
directory along with any images and converted to TeX format.
`HTML::TreeBuilder` class is used to extract the html content.
html tags are converted to TeX macros using regex.
